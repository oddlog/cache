export default RingCache;

/*===================================================== Classes  =====================================================*/

function RingCache(limit, cleanUp, freeUp) {
  this.limit = limit;
  this.cleanUp = cleanUp;
  this.freeUp = freeUp;
  this.lostRecords = 0;
  this.logsStart = 0;
  this.logsEnd = 0;
  this.logs = null;
}

RingCache.prototype.push = function (log) {
  let logs = this.logs;
  if (logs == null) { logs = this._lazyCreateLogsArray(); }
  logs[this.logsEnd++] = log;
  if (this.logsEnd === logs.length) { this.logsEnd = 0; }
  if (this.logsEnd === this.logsStart) {
    this.logsStart++;
    if (this.logsStart === logs.length) { this.logsStart = 0; }
    this.lostRecords++;
  }
};

RingCache.prototype.child = function () { return new RingCache(this.limit, this.cleanUp, this.freeUp); };

RingCache.prototype.flood = function (log, writeFn) {
  const logs = this.logs, end = this.logsEnd, start = this.logsStart;
  let floodSize;
  if (logs != null) {
    if (start > end) {
      for (let i = start; i < logs.length; i++) { writeFn(logs[i]); }
      for (let i = 0; i < end; i++) { writeFn(logs[i]); }
      floodSize = logs.length - start + end + 1;
    } else {
      for (let i = start; i < end; i++) { writeFn(logs[i]); }
      floodSize = end - start + 1;
    }
  } else {
    floodSize = 1;
  }
  writeFn(log);
  if (this.cleanUp) {
    this.lostRecords = 0;
    this.logsEnd = start;
    if (this.freeUp) { this.logs = null; }
  } else {
    this.push(log);
  }
  return {
    traced: this.lostRecords + floodSize,
    backed: floodSize,
    lost: this.lostRecords,
    limit: this.limit,
  };
};

RingCache.prototype._lazyCreateLogsArray = function () { return this.logs = new Array(this.limit + 1); };
