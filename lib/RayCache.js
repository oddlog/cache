export default RayCache;

/*===================================================== Classes  =====================================================*/

function RayCache(limit, cleanUp, malloc, windowSize, silentWindow) {
  this.limit = limit;
  this.cleanUp = cleanUp;
  this.malloc = malloc;
  this.windowSize = windowSize;
  this.capSize = limit + windowSize;
  this.silentWindow = silentWindow;
  this.lostRecords = 0;
  this.logsSize = 0;
  this.logs = null;
}

RayCache.prototype.push = function (log) {
  let logs = this.logs;
  if (logs == null) { logs = this._lazyCreateLogsArray(); }
  logs.push(log);
  if (++this.logsSize === this.capSize) {
    const drop = logs.splice(0, this.windowSize);
    this.logsSize -= drop.length;
    this.lostRecords += drop.length;
  }
};

RayCache.prototype.child = function () {
  return new RayCache(this.limit, this.cleanUp, this.malloc, this.windowSize, this.silentWindow);
};

RayCache.prototype.flood = function (log, writeFn) {
  const logs = this.logs;
  if (logs != null) {
    const _len = this.logsSize;
    for (let i = (this.silentWindow && _len > this.limit ? _len - this.limit : 0); i < _len; i++) {
      writeFn(logs[i]);
    }
  }
  writeFn(log);
  if (this.cleanUp) {
    this.lostRecords = 0;
    this.logsSize = 0;
    this.logs = null;
  } else {
    this.push(log);
  }
  return {
    traced: this.lostRecords + this.logsSize + 1,
    backed: this.logsSize + 1,
    lost: this.lostRecords,
    limit: this.limit,
    window: this.windowSize,
  };
};

RayCache.prototype._lazyCreateLogsArray = function () {
  return this.logs = typeof this.malloc === "number" ? new Array(this.malloc) : [];
};
