import RingCache from "./RingCache";
import RayCache from "./RayCache";

const DEFAULT_LIMIT = 100;
const MAX_AUTO_ALLOCATE_LIMIT = 4096;

/*===================================================== Exports  =====================================================*/

export {RingCache, RayCache, create};

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new cache according to the passed options.
 *
 * @param {Object} options The options:
 * <ul>
 *   <li>`{Number} [limit=50]` The cache size.</li>
 *   <li>`{Boolean|Number} [window=(limit >= 4096)]` If `false`, a pre-allocated array with the `limit` as length is
 *       used with performance-optimized operations. Otherwise it determines the amount of additional cache size for
 *       discarding multiple logs at once; `true` is interpreted as `max(5, limit/20)`.</li>
 *   <li>`{Boolean} [silentWindow=false]` Affects only if `window !== false`. Whether to consider the cache discard
 *       window as already discarded and skip during flood.</li>
 *   <li>`{Number} [malloc]` Affects only if `window !== false`. The size of the initial cache array.</li>
 *   <li>`{Boolean} [cleanUp=true]` Whether to clean up the cache after each flood.</li>
 *   <li>`{Boolean} [freeUp=cleanUp]` Affects only if `window === false` and `cleanUp === true`. Whether to free up the
 *       cache for garbage collection after each flood. Disable for extreme performance when the cache would get
 *       overwritten shortly after flood anyways.</li>
 * </ul>
 * @returns {{push: Function, child: Function, flood: Function}} The cache instance that got derived from the options.
 */
function create(options) {
  const limit = typeof options.limit === "number" ? options.limit : DEFAULT_LIMIT;
  const cleanUp = options.cleanUp !== false;
  const window = options.hasOwnProperty("window") ? options.window : limit > MAX_AUTO_ALLOCATE_LIMIT;
  if (window === false) {
    const freeUp = cleanUp && (options.hasOwnProperty("freeUp") ? options.freeUp : true);
    return new RingCache(limit, cleanUp, freeUp);
  } else {
    const windowSize = window === true ? (limit > 100 ? (limit / 20) | 0 : 5) : window;
    const silentWindow = !!options.silentWindow;
    return new RayCache(limit, cleanUp, options.malloc, windowSize, silentWindow);
  }
}
